<?php declare(strict_types=1);

namespace Fittinq\Pimcore\Commands\Exception;

use RuntimeException;

class CommandNotFoundException extends RuntimeException
{

}