<?php

declare(strict_types=1);

namespace Test\Fittinq\Pimcore\Commands\Mock;

use Pimcore\Model\Exception\UnsupportedException;
use Symfony\Component\Lock\Key;
use Symfony\Component\Lock\PersistingStoreInterface;

class StoreMock implements PersistingStoreInterface
{
    public function save(Key $key)
    {
        throw new UnsupportedException('function not implemented');
    }

    public function delete(Key $key)
    {
        throw new UnsupportedException('function not implemented');
    }

    public function exists(Key $key): bool
    {
        throw new UnsupportedException('function not implemented');
    }

    public function putOffExpiration(Key $key, float $ttl)
    {
        throw new UnsupportedException('function not implemented');
    }
}